from django.test import TestCase
from vocab.views import home,search,sort,index
from django.core.urlresolvers import resolve
class HomePageTest(TestCase):
    def test_root_url_resolves_to_home_view(self):
        found = resolve('/') 
        self.assertEqual(found.func, home)

    def test_root_url_resolves_to_index_view(self):
        found = resolve('/vocab/') 
        self.assertEqual(found.func, index)

    def test_root_url_resolves_to_search_view(self):
        found = resolve('/vocab/search') 
        self.assertEqual(found.func, search)

    def test_root_url_resolves_to_sort_view(self):
        found = resolve('/vocab/sort') 
        self.assertEqual(found.func, sort)
