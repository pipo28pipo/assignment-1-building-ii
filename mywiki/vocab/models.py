from django.db import models

# Create your models here.

class Word(models.Model):
    word_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.word_text

class Info(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    type_text = models.CharField(max_length=200)
    meaning_text = models.CharField(max_length=200)
    sentence_text = models.CharField(max_length=200)
    def __str__(self):
        return self.type_text

class SearchFrequency(models.Model):
    frequency = models.CharField(max_length=200)
    def __str__(self):
        return self.frequency

class SearchPopular(models.Model):
    search_word = models.CharField(max_length=200)
    search = models.IntegerField(default=1)
    def __str__(self):
        return self.search_word

