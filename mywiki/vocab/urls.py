from django.conf.urls import url

from . import views
app_name = 'wiki'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search$', views.search, name='search'),
    url(r'^searched$', views.searched, name='searched'),
    url(r'^deposit$', views.deposit, name='deposit'),
    url(r'^get_word$', views.get_word, name='get_word'),
    url(r'^upload$', views.upload, name='upload'),
    url(r'^uploaded$', views.uploaded, name='uploaded'),
]
